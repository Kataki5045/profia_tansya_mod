# グラプロ・プロフィア内装 [![pipeline status](https://gitlab.com/offworld.ets/profia_tansya_mod/badges/master/pipeline.svg)](https://gitlab.com/offworld.ets/profia_tansya_mod/-/commits/master)                    

**グラプロ・プロフィア内装　by Offworld([@offworld99](https://twitter.com/offworld99)) & Honrai([@honrai_zi](https://twitter.com/honrai_zi))**

martinさんのグランドプロフィア、himopanさんのプロフィア用の内装です。前期と後期の二種類のメーターが入っています。
ほんらいさんの内装サウンドも組み込ませて頂いております。
Shnaさんのグラプロ修正にも対応しています。

優先順位はこちらのMODを上にしてください。

グラプロ、もちくはプロフィアのMODが入っていない場合は、def/vehicle/truck から入っていないトラックのファイルを抜いてください。

[**ダウンロード**](https://gitlab.com/offworld.ets/profia_tansya_mod/-/jobs/artifacts/master/raw/FILE_NAME.zip?job=build)    
